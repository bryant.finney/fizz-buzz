# `fizz-buzz`

Implement a [fizz-buzz](https://en.wikipedia.org/wiki/Fizz_buzz) application, and manage
its deployment to AWS.

## Overview

The application is implemented using the
[`FastAPI`](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html) web
framework.

## Design Decisions

- ~~Python `3.9` was selected during 48f6cc99b5 as the interpreter version according to
  supported AWS lambda runtimes~~
  - ref: https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html
  - after starting the `serverless` integration, the Python version was downgraded to
    `3.8`
- the [`serverless`](https://adem.sh/blog/tutorial-fastapi-aws-lambda-serverless)
  framework was selected for deploying the application
  - ref: https://adem.sh/blog/tutorial-fastapi-aws-lambda-serverless
- the `FastAPI` was selected for the framework due to the size / complexity of the
  application (small with minimal functionality)
- an initial deployment identified an error was preventing the root path `/` from being
  used as a valid endpoint; accordingly, the primary endpoint provided by this app
  was moved to `/fizzbuzz`

## Setup and Deployment

- install [`direnv`](https://direnv.net/docs/installation.html),
  [`poetry`](https://python-poetry.org/docs/#installation), and
  [`node v17.0.1`](https://nodejs.org/en/blog/release/v17.0.1/)
- run `direnv allow` to initialize the development environment
- to run the app locally: `python -m fizz_buzz`
- to deploy the app: `npx sls deploy`

## API Documentation

When running locally, two endpoints provide the API documentation:

- [/docs](http://localhost:8000/docs/) hosts a Swagger UI rendered from the OpenAPI
  schema
- [/redoc](http://localhost:8000/redoc/) provides the schema rendered using
  [ReDoc](https://github.com/Redocly/redoc)

These pages are also available when the app has been deployed.

Additionally, [openapi.json](openapi.json) was retrieved from the `dev` deployment
stage, which is accessible at the following URL:
https://dp1otzpozd.execute-api.us-east-2.amazonaws.com/dev
