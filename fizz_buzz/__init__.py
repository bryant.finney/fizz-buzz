"""This package implements a fizzbuzz web application."""
# stdlib
import logging
import os

# third party
from fastapi import FastAPI
from mangum import Mangum
from pydantic import BaseModel

__version__ = "0.0.0"

logger = logging.getLogger("fizz_buzz")


class FizzBuzzResponseModel(BaseModel):
    """The result is 'Fizz', 'Buzz', 'FizzBuzz', or the provided number as a string."""

    result: str


class VersionResponseModel(BaseModel):
    """Provide the version of the application."""

    version: str


prefix = os.environ.get("STAGE", "")
app = FastAPI(title="FinneyFizzBuzz", openapi_prefix=f"/{prefix}" if prefix else None)


def get_response(number: int) -> str:
    """Calculate the appropriate response for the fizzbuzz game.

    Usage:
        >>> get_response(4)
        '4'

        >>> get_response(5)
        'Buzz'

        >>> get_response(9)
        'Fizz'

        >>> get_response(15)
        'FizzBuzz'

        >>> get_response(5426)
        '5426'

    Args:
        number (int): the number to test

    Returns:
        str: the appropriate response; one of 'Fizz', 'Buzz', 'FizzBuzz', or
            the given value of number (cast to a string)
    """
    if number % 15 == 0:
        return "FizzBuzz"

    if number % 5 == 0:
        return "Buzz"

    if number % 3 == 0:
        return "Fizz"

    return str(number)


@app.get("/fizzbuzz", response_model=FizzBuzzResponseModel)
def fizz_buzz(number: int) -> FizzBuzzResponseModel:
    """Get the response to the FizzBuzz game.

    # Rules

    - if the provided number is divisible by `3`, response is `'Fizz'`
    - if it's divisible by `5`, the response is `'Buzz'`
    - if it's divisible by both: `'FizzBuzz'`
    - otherwise, the response is the given number
    """
    return FizzBuzzResponseModel(result=get_response(number))


@app.get("/version", response_model=VersionResponseModel)
def version() -> VersionResponseModel:
    """Get the version of the application."""
    return VersionResponseModel(version=__version__)


handler = Mangum(app)

logger.debug("imported module %s", __name__)
