"""Define the ``fizz_buzz`` application functionality.

.. moduleauthor:: Bryant Finney
   :github: https://bryant-finney.github.io/about
"""
# stdlib
import logging
import sys

# third party
from uvicorn import main

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    if "fizz_buzz:app" not in sys.argv[1:]:
        sys.argv.append("fizz_buzz:app")
    main()  # pylint: disable=no-value-for-parameter

logger.debug("imported module %s", __name__)
